var express = require('express'),
    encrypt = require('../service/encrypt'),
    dotenv = require('dotenv').config(),
    unirequest = require('../service/unirest-service');


  var baseurl = process.env.BASE_URL;
  var apikey = process.env.API_KEY;
  var merchantid = process.env.SECRET_KEY;

  module.exports = {
      chargeCard : function(req, res, next){
        var cardDetails = {
          "amount": encrypt(apikey, req.query.amount),
          "authmodel": encrypt(apikey, "VBVSECURECODE"),
          "cardno": encrypt(apikey, req.query.cardnumber),
          "currency": encrypt(apikey, req.query.currency),
          "cvv": encrypt(apikey, req.query.cvv),
          "expirymonth": encrypt(apikey, req.query.expirymonth),
          "expiryyear": encrypt(apikey, req.query.expiryyear),
          "merchantid": merchantid,
          "narration": encrypt(apikey, req.query.description),
          "responseurl": encrypt(apikey, "http:127.0.0.1:3030/confirm")
        };

        var request_object = {
          baseurl: baseurl,
          path: "card/mvva/pay",
          headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
          payload: cardDetails
        };

        unirequest(request_object)
        .then((response) => {
          if (response.body.status == "success" && response.body.data.responsecode == "02") {
            var authUrl = response.body.data.authurl;
            // console.log(response.body);
            res.json({data: authUrl});
          }
        })
        .catch((err) => {
          res.json({error: err.message});
        })

      }
  };
