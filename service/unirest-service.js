var q = require('q');
var unirest = require('unirest');


function request(RequestConfig) {

  // set request skeleton for all unirest calls, only style ease so you don't have to rewrite unirest in all files.

  var method = RequestConfig.method || "post";

  var localReqConfig = RequestConfig;

  localReqConfig.baseurl = RequestConfig.baseurl || "http://staging1flutterwave.co:8080/pwc/rest/";
  localReqConfig.path = RequestConfig.path || "";
  localReqConfig.headers = RequestConfig.headers || {'Content-Type': 'application/json'};
  localReqConfig.payload = RequestConfig.payload || {};

  // build request url
  var request_url = localReqConfig.baseurl + localReqConfig.path;

  var d = q.defer();

  unirest[method](request_url)
  .headers(localReqConfig.headers)
  .send(localReqConfig.payload)
  .end((response) => {
    if (response.body.status == "success" && response.body.data.responsecode == "02") {
      d.resolve(response);
    } else {
      d.reject(new Error("Error Charging card: " +response.body.data.responsemessage));
    }

  });
  return d.promise;
}

module.exports = request;
