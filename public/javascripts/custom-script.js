$(function (){

  $('#flw-btnpay').on('click', function () {
    function validate (){
      var params = {
        amount: $('#flw_amnt').val(),
        cardnumber: $('#flw_ccno').val(),
        currency: $('#flw_currency').val(),
        cvv: $('#flw_cvv').val(),
        expirymonth: $('#flw_ccmonth').val(),
        expiryyear: $('#flw_ccyear').val(),
        description: $('#flw_desc').val()
      };

      $.ajax({
        contentType: 'application/json',
        dataType: 'json',
        method: 'GET',
        cache: false,
        data: params,
        url: '/card/pay'
      }).done(function(resp) {
        d.resolve(resp);
      }, function(err) {
        d.reject(err);
      })

    }
    validate();
  });

});


  // Create a Deffered object

  var d = $.Deferred();
  var url;
d.done(function(response){
     url = response.data;
  })

  function newIframe () {
    ifrm = document.createElement("IFRAME");
    ifrm.setAttribute('src', url);
    ifrm.style.width = 600+"px";
    ifrm.style.height = 500+"px";
    document.getElementById('appendiframe').appendChild(ifrm);
  }
