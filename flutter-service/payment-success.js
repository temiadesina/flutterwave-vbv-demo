var express = require('express');

module.exports = {
  confirmPayment : function (req, res) {
    var response = req.query;
    var responseArray = JSON.parse(response.resp);

    if (responseArray.responsecode == "0" || responseArray.responsecode == "00") {
       res.render('successful');
    } else {
      var msg = responseArray.responsemessage;
      res.render('unsuccessful', {message: msg});
    }
  }
}
