var express = require('express');
var router = express.Router();
var Charge = require('../flutter-service/card-pay');
var Confirm = require('../flutter-service/payment-success');


router.get('/card/pay', Charge.chargeCard);
router.get('/confirm', Confirm.confirmPayment);




module.exports = router;
